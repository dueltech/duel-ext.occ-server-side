const gulp = require('gulp');
const zip = require('gulp-zip');

gulp.task('zip', () => {
  return gulp.src(['./duelServerSide/**'])
    .pipe(zip('duelServerSide.zip'))
    .pipe(gulp.dest('./dist'));
});
