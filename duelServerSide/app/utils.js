/**
 * Formats products for Duel catalog import
 * @param {Object} data Data/body as received from /ccadmin/v1/priceListGroups of the OCC API
 * @param {string} [priceGroupId] User selected price group ID (optional)
 * @return {Currencies} Currency preferences of the shop
 */
module.exports.formatCurrencies = (data, priceGroupId = null) => {
  const defaultCurrency = data.defaultPriceListGroup.currency.currencyCode;
  let selectedCurrency;

  if (priceGroupId) {
    // if there's a price group provided, get its currency
    const selectedGroup = data.items.find(group => (
      group.repositoryId === priceGroupId
    ));

    if (selectedGroup) selectedCurrency = selectedGroup.currency.currencyCode;
  }

  // default & selected are needed in case a product doesn't have a price for the selected currency
  const currencies = {
    default: defaultCurrency,
  };
  if (selectedCurrency) currencies.selected = selectedCurrency;

  return currencies;
};

/**
 * Formats products for Duel catalog import
 * @param {Object[]} items Items as received from the products endpoint of the OCC API
 * @param {Object} settings Configuration settings
 * @param {Currencies} settings.currencies Currency preferences
 * @param {string} settings.storeUrl The store's public base URL
 * @param {string} [settings.priceGroup] The selected price group ID (optional)
 * @return {Object[]} Formatted items
 */
module.exports.formatProducts = (items, settings) => (
  // put the products in a nice format will all the needed information
  items.map((product) => {
    let price;
    // try to get the sale price or list price of the given price group
    if (settings.priceGroup && settings.currencies.selected) {
      const salePrice = product.salePrices[settings.priceGroup];
      const listPrice = product.listPrices[settings.priceGroup];
      price = salePrice || listPrice;
    }
    // get selected price group's or default currency code,
    // depending on whether or not there's a price yet
    const currency = price ? settings.currencies.selected : settings.currencies.default;

    if (!price) {
      // there's neither sale nor list price for that price group, fall back to default prices
      price = product.salePrice || product.listPrice;
    }

    return {
      name: product.displayName,
      sku: product.id,
      description: product.description,
      url: settings.storeUrl + product.route,
      srcImg: settings.storeUrl + product.primaryFullImageURL,
      price,
      currency,
    };
  })
);

/**
 * @typedef {Object} Currencies
 * @property {string} default The shop's default currency
 * @property {string} [selected] The user selected currency (optional)
 */
