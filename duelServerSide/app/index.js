const express = require('express');
const nconf = require('nconf');
const CommerceSDK = require('./commerce-rest');
const utils = require('./utils');

nconf.argv().env();

// Oracle Commerce Cloud has env variables already set, local setup doesn't
if (!nconf.get('atg.server.url')) {
  nconf.file({ file: 'config.json' });
}

// The public store URL - will be used to compose image URLs and other publicly reachable links
const storeUrl = nconf.get('atg.server.url');
// The admin store URL - will be used to authenticate and make requests to the Admin API there
const adminUrl = nconf.get('atg.server.admin.url');
// The application key - will be exchanged for a token to make requests to the API
const apiKey = nconf.get('atg.application.credentials')['atg.application.token'];

const storeSDK = new CommerceSDK({
  hostname: adminUrl,
  port: 443,
  apiKey,
});

// duelsettings contain the selected collection and currency in the admin dashboard
const getSettings = () => (
  new Promise((resolve, reject) => {
    storeSDK.get({
      url: '/ccadmin/v1/sitesettings/duelsettings',
      callback: (err, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body.data);
        }
      },
    });
  })
);

// fetch all currencies and filter out currency codes for the selected & default one
const addCurrencies = settings => (
  new Promise((resolve, reject) => {
    storeSDK.get({
      url: '/ccadmin/v1/priceListGroups',
      callback: (err, body) => {
        if (err) {
          reject(err);
        } else {
          const currencies = utils.formatCurrencies(body, settings.duelPriceGroupId);
          resolve({
            duelCollectionId: settings.duelCollectionId,
            duelPriceGroupId: settings.duelPriceGroupId,
            currencies,
          });
        }
      },
    });
  })
);

const fetchProducts = (settings, offset = 0) => (
  new Promise((resolve, reject) => {
    // use whole catalog if no collection id was specified
    const category = `${settings.duelCollectionId ? `?categoryId=${settings.duelCollectionId}&` : ''}`;
    storeSDK.get({
      url: `/ccadmin/v1/products?${category}offset=${offset}`,
      headers: {
        'X-CCProfileType': 'adminUI',
      },
      callback: (err, body) => {
        if (err) {
          reject(err);
        } else {
          resolve(body);
        }
      },
    });
  })
);

// fetch products from specified collection & format them for the Duel catalog import
const getProducts = settings => (
  new Promise((resolve, reject) => {
    fetchProducts(settings)
      .then((body) => {
        const formatProducts = items => (
          utils.formatProducts(items, {
            priceGroup: settings.duelPriceGroupId,
            currencies: settings.currencies,
            storeUrl,
          })
        );
        let products = formatProducts(body.items);
        if (body.totalResults > body.limit) {
          const promises = [];
          for (let i = 1; i < Math.ceil(body.totalResults / body.limit); i += 1) {
            promises.push(fetchProducts(settings, body.limit * i));
          }
          Promise.all(promises)
            .then((responses) => {
              responses.forEach((res) => {
                products = products.concat(formatProducts(res.items));
              });
              resolve(products);
            })
            .catch(err => reject(err));
        } else {
          resolve(products);
        }
      })
      .catch(err => reject(err));
  })
);

const app = express();

// Product feed for the Duel catalog import
app.get('/duel/products', (req, res) => {
  getSettings()
    .then(settings => addCurrencies(settings))
    .then(settings => getProducts(settings))
    .then((products) => {
      storeSDK.logout();
      return res.json({ items: products });
    })
    .catch((error) => {
      storeSDK.logout();
      res.locals.logger.error(error);
      return res.status(500).json({ error: error.message });
    });
});

// Export app in order to be embedded in OCCS server-side extension architecture
module.exports = app;
