const request = require('request-promise');
require('dotenv').config();

/**
 * Authenticates with admin credentials & resolves with the token
 * @returns {Promise<string>} Access token
 */
exports.getToken = () => (
  request.post({
    url: `${process.env.OCCS_DOMAIN}/ccadminui/v1/login`,
    form: {
      grant_type: 'password',
      username: process.env.ADMIN_USERNAME,
      password: process.env.ADMIN_PASSWORD,
    },
  })
    .then(result => JSON.parse(result).access_token)
    .catch(err => console.error(err))
);
