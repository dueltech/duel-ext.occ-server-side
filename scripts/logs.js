const fs = require('fs');
const path = require('path');
const request = require('request-promise');
const auth = require('./utils/adminAuth.js');
require('dotenv').config();

const validArgs = ['error', 'warning', 'debug', 'info'];

const writeLog = (dir, level, body) => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  let fileExt;
  try {
    JSON.parse(body);
    fileExt = 'json';
  } catch (error) {
    fileExt = 'html';
  }
  fs.writeFile(`${dir}/${level}-${Date.now()}.${fileExt}`, body, (err) => {
    if (err) throw err;
  });
};

const deleteLogs = (dir) => {
  fs.readdir(dir, (err, files) => {
    if (err) throw err;

    files.forEach((file) => {
      fs.unlink(path.join(dir, file), (error) => {
        if (error) throw error;
      });
    });
  });
};

auth.getToken().then((token) => {
  const arg = process.argv[2];
  const logsDir = path.join(__dirname, '../logs');

  if (arg === 'clear') return deleteLogs(logsDir);

  if (arg && validArgs.indexOf(arg) === -1) {
    return console.error(`Error: Invalid logging level "${arg}" - must be one of ${validArgs.join(', ')}`);
  }

  const level = arg || 'error';

  return request.get({
    url: `${process.env.OCCS_DOMAIN}/ccadminx/custom/v1/logs?loggingLevel=${level}`,
    auth: {
      bearer: token,
    },
  }).then((body) => {
    writeLog(logsDir, level, body);
  }).catch((err) => { console.error(`Error: ${err.message}`); });
});

